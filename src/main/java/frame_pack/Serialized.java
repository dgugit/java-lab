package frame_pack;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public interface Serialized
{
    void serializeJSON(Frame object,String name) throws IOException;
    Frame deserializeJSON(String name)throws IOException;

    void serializeXML(Frame object, String name) throws JAXBException;
    Frame deserializeXML(String name) throws IOException;

    void serializeTXT(Frame frame, String path) throws IOException;
    Frame deserializeTXT( String filepath) throws IOException;
}
