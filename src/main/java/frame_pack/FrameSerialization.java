package frame_pack;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import javax.xml.bind.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FrameSerialization implements Serialized
{

    public void serializeJSON(Frame frame, String filepath)
    {
        File file = new File(filepath);
        ObjectMapper objMapper = new ObjectMapper();
        objMapper.configure(com.fasterxml.jackson.databind.SerializationFeature.INDENT_OUTPUT, true);
        StringWriter stringDep = new StringWriter();
        try {
            objMapper.writeValue(stringDep, frame);
            FileWriter fw = new FileWriter(file);
            fw.write(stringDep.toString());
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void serializeXML(Frame object, String filepath) throws JAXBException
    {
        JAXBContext jaxbContext = JAXBContext.newInstance(Frame.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        File file = new File(filepath);
        try
        {
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(object, file);
        }
        catch (JAXBException e)
        {
            e.printStackTrace();
        }

    }

    public Frame deserializeJSON(String filePath)
    {
        File file = new File(filePath);
        Frame frame;
        byte[] mapData = new byte[0];
        try
        {
            mapData = Files.readAllBytes(Paths.get(file.toString()));
            ObjectMapper objectMapper = new ObjectMapper();
            frame = objectMapper.readValue(mapData, Frame.class);
            return frame;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public Frame deserializeXML(String filePath)
    {
        JAXBContext jaxbContext = null;
        File file = new File(filePath);
        try
        {
            jaxbContext = JAXBContext.newInstance(Frame.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Frame emps = (Frame) jaxbUnmarshaller.unmarshal(file);
            return emps;
        }
        catch (JAXBException e)
        {
            e.printStackTrace();
        }
        return new Frame();
    }

    public void serializeTXT(Frame frame, String filePath) throws IOException
    {
        BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));
        writer.write(frame.toString());
        if (writer != null)
            writer.close();
    }

    public Frame deserializeTXT(String filepath) throws IOException
    {
        FileInputStream inFile = new FileInputStream(filepath);
        byte[] str = new byte[inFile.available()];
        inFile.read(str);

        String text = new String(str);
        String[] mas = text.split("\n");

        List<frameElement> el =  new ArrayList<>();
        int i= 0;
        int lastI = 0;
        frameElement fm ;

//        System.out.println(mas[4]+" "+ mas[8]+ " " +mas[12] );


        try {
            i=1;
            do{
                switch (mas[i])
                {
                    case "class frame_pack.Baget":
                        {
                            fm = new Baget();
                            float heigth = Float.parseFloat(mas[++i]);
                            float width = Float.parseFloat(mas[++i]);
                            float price = Float.parseFloat(mas[++i]);
                            fm.setHeight(heigth);
                            fm.setWidth(width);
                            fm.setOnePrice(price);
                            el.add(fm);
                            fm=null;
                            lastI = ++i;
                            break;
                    }

                    case "class frame_pack.Fanera":
                        {
                        fm = new Fanera();
                        float heigth = Float.parseFloat(mas[++i]);
                        float width = Float.parseFloat(mas[++i]);
                        float price = Float.parseFloat(mas[++i]);
                        fm.setHeight(heigth);
                        fm.setWidth(width);
                        fm.setOnePrice(price);
                        el.add(fm);
                        lastI = ++i;
                        break;
                    }

                    case "class frame_pack.Glass":
                        {
                        fm = new Glass();
                        float heigth = Float.parseFloat(mas[++i]);
                        float width = Float.parseFloat(mas[++i]);
                        float price = Float.parseFloat(mas[++i]);
                        fm.setHeight(heigth);
                        fm.setWidth(width);
                        fm.setOnePrice(price);
                        el.add(fm);
                        lastI = ++i;
                        break;
                    }
                    case "frame": break;
                    //throw new IllegalArgumentException("Invalid frameElements deserialization");

                }

            }while(!mas[lastI].equalsIgnoreCase("frame"));

        }
        catch (Exception e)
        {  e.printStackTrace();}

//        String result = " ";
//        for( frameElement f: el)
//        {
//            result += "\n" + f.getClass().toString();
//            result+="\n" + f.getHeight();
//            result+="\n" + f.getWidth();
//            result+="\n" + f.getPerOnePrice();
//        }
//        System.out.println( "We created list: " + "\n" +result);

        //System.out.println( " There have to be word 'frame' : " + "[" + mas[lastI]+"]");
        //if(mas[lastI].equalsIgnoreCase("frame"))
        Frame frame = new Frame.Builder()
                    .setElements(el)
                    .setHeight(Float.parseFloat(mas[++lastI]))
                    .setWidth(Float.parseFloat(mas[++lastI]))
                    .setPerOnePriceFrame(Float.parseFloat(mas[++lastI]))
                    .build();

        System.out.println("\nCreated obj:" + "\n"+ frame);
        return frame;
    }

    public static void main(String [] args)
    {
        //Create object
        FrameSerialization iniz = new FrameSerialization();
        Frame frame = new Frame(40,30);
        frame.setOrder(true,true,true);

        System.out.println("Our object: " + "\n" + frame);

//        //Json serialization

//          iniz.serializeJSON(frame, "out.json");

//        //JSON deserialization

//           Frame frameD = iniz.deserializeJSON("out.json");
//           System.out.println("JSON deserialized "+ frameD);

        //XML Serialization

//            try
//            {
//
//                iniz.serializeXML(frame,"out.xml");
//
//            } catch (JAXBException e){
//                e.printStackTrace();
//            }

//        //XML Deserislization

//            Frame frameXD = iniz.deserializeXML("out.xml");
//            System.out.println("XML unmarshalling:" + frameXD);

        //TXT serialization
//        try
//        {
//            iniz.serializeTXT(frame, "out.txt");
//        }
//        catch(IOException e)
//        {
//            e.printStackTrace();
//        }
//
//
//        try
//        {
//            iniz.deserializeTXT("out.txt");
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }
    }
}
